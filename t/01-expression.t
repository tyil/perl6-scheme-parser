#! /usr/bin/env perl6

use v6.c;

use Scheme::Parser;
use Scheme::Parser::Actions;
use Scheme::Parser::Env::Default;
use Test;

plan 3;

my $parser = Scheme::Parser.new;
my $actions = Scheme::Parser::Actions.new(env => Scheme::Parser::Env::Default.new);

subtest "(+ 2 3)" => {
	plan 5;

	my $scheme = $parser.parse("(+ 2 3)", :$actions);

	is ~$scheme<expression>, "(+ 2 3)", "Expression extracted";
	is ~$scheme<expression><symbol>[0].made, "+", "Symbol + extracted";
	is $scheme<expression><symbol>[1].made, 2, "Symbol 2 extracted";
	is $scheme<expression><symbol>[2].made, 3, "Symbol 3 extracted";
	is $scheme.made, 5, "Outcome is correct";
}

subtest "( + 4 5 ) " => {
	plan 5;

	my $scheme = $parser.parse("( + 4 5 ) ", :$actions);

	is ~$scheme<expression>, "( + 4 5 ) ", "Expression extracted";
	is ~$scheme<expression><symbol>[0].made, "+", "Symbol + extracted";
	is $scheme<expression><symbol>[1].made, 4, "Symbol 4 extracted";
	is $scheme<expression><symbol>[2].made, 5, "Symbol 5 extracted";
	is $scheme.made, 9, "Outcome is correct";
}

subtest "(- 6 (+ 1 4))" => {
	plan 8;

	my $scheme = $parser.parse("(- 6 (+ 1 4))", :$actions);

	is ~$scheme<expression>, "(- 6 (+ 1 4))", "Expression extracted";
	is ~$scheme<expression><symbol>[0].made, "-", "Symbol + extracted";
	is $scheme<expression><symbol>[1].made, 6, "Symbol 6 extracted";
	is ~$scheme<expression><symbol>[2], "(+ 1 4)", "Expression (+ 1 4) extracted";
	is ~$scheme<expression><symbol>[2]<expression><symbol>[0].made, "+", "Symbol + extracted";
	is $scheme<expression><symbol>[2]<expression><symbol>[1].made, 1, "Symbol 1 extracted";
	is $scheme<expression><symbol>[2]<expression><symbol>[2].made, 4, "Symbol 4 extracted";
	is $scheme.made, 1, "Outcome is correct";
}

# vim: ft=perl6 noet
