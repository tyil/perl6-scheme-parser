#! /usr/bin/env perl6

use v6.c;

use Scheme::Parser;
use Scheme::Parser::Actions;
use Scheme::Parser::Env::Default;
use Test;

plan 2;

my $parser = Scheme::Parser.new;
my $actions = Scheme::Parser::Actions.new(env => Scheme::Parser::Env::Default.new);

subtest "(if (< 1 2) 1 2)" => {
	plan 1;

	my $scheme = $parser.parse("(if (< 1 2) 1 2)", :$actions);

	is $scheme.made, 1, "If condition succeeds";
}

subtest "(if (> 1 2) 1 2)" => {
	plan 1;

	my $scheme = $parser.parse("(if (> 1 2) 1 2)", :$actions);

	is $scheme.made, 2, "If condition succeeds";
}

# vim: ft=perl6 noet
