#! /usr/bin/env perl6

use v6.c;

use Scheme::Parser;
use Scheme::Parser::Actions;
use Scheme::Parser::Env::Default;
use Test;

plan 2;

my $parser = Scheme::Parser.new;

subtest "(define r 10)" => {
	plan 3;

	my $actions = Scheme::Parser::Actions.new(env => Scheme::Parser::Env::Default.new);

	ok my $scheme = $parser.parse("(define r 10)", :$actions), "Parses the define with no errors";
	ok $actions.env.has-variable("r"), "r is defined";
	is $actions.env.get-variable("r"), 10, "r is defined as 10";
}

subtest "(define r (+ 1 1))" => {
	plan 3;

	my $actions = Scheme::Parser::Actions.new(env => Scheme::Parser::Env::Default.new);

	ok my $scheme = $parser.parse("(define r (+ 1 1))", :$actions), "Parses a define with expression as value";
	ok $actions.env.has-variable("r"), "r is defined";
	is $actions.env.get-variable("r"), 2, "r is defined as 2";
}

# TODO: Test redefines
# TODO: Use a defined var

# vim: ft=perl6 noet
