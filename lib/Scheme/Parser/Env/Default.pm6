#! /usr/bin/env false

use v6.c;

use Scheme::Parser::Env;

class Scheme::Parser::Env::Default is Scheme::Parser::Env
{
	method TWEAK
	{
		self.add-procedure: "+" => sub ($a, $b) { $a + $b };
		self.add-procedure: "-" => sub ($a, $b) { $a - $b };
		self.add-procedure: "<" => sub ($a, $b) { $a < $b };
		self.add-procedure: ">" => sub ($a, $b) { $a > $b };

		self.add-variable: "π", π;
		self.add-variable: "pi", π;
	}
}

=begin pod

=NAME    Scheme::Parser::Env::Default
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 EXAMPLES

=head1 SEE ALSO

=end pod

# vim: ft=perl6 noet
