#! /usr/bin/env false

use v6.c;

class Scheme::Parser::Env
{
	has %.procedures;
	has %.variables;

	multi method add-procedure (
		Str:D $name,
		&callable,
	) {
		%!procedures{$name} = &callable;
	}

	multi method add-procedure (
		Pair $proc where { $_.key ~~ Str:D && $_.value ~~ Callable:D }
	) {
		%!procedures{$proc.key} = $proc.value;
	}

	method get-procedure (
		Str:D $name,
		--> Callable
	) {
		%!procedures{$name};
	}

	method has-procedure (
		Str:D $name,
		--> Bool
	) {
		%!procedures{$name}:exists;
	}

	method run-procedure (
		Str:D $name,
		*@args,
		--> Any
	) {
		%!procedures{$name}(|@args);
	}

	method add-variable (
		Str:D $name,
		Any:D $value,
	) {
		%!variables{$name} = $value;
	}

	method get-variable (
		Str:D $name,
		--> Any
	) {
		%!variables{$name};
	}

	method has-variable (
		Str:D $name,
		--> Bool
	) {
		%!variables{$name}:exists;
	}
}

=begin pod

=NAME    Scheme::Parser::Env
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 EXAMPLES

=head1 SEE ALSO

=end pod

# vim: ft=perl6 noet
