#! /usr/bin/env false

use v6.c;

use Scheme::Parser::Env;

class Scheme::Parser::Actions
{
	has Scheme::Parser::Env $.env;

	method TOP ($/)
	{
		make $<expression>.made;
	}

	multi method expression ($/)
	{
		die "No such procedure: {$<symbol>[0].made}" unless $!env.has-procedure($<symbol>[0].made);

		make $!env.run-procedure(|$<symbol>».made);
	}

	multi method expression (
		$/ where { $<symbol>[0].made eq "define" }
	) {
		$!env.add-variable(|$<symbol>[1 .. 2]».made);
	}

	multi method expression (
		$/ where { $<symbol>[0].made eq "quote" }
	) {
		make ~$<symbol>[1];
	}

	multi method expression (
		$/ where { $<symbol>.head.made eq "if" }
	) {
		my ($condition, $true, $false) = $<symbol>[1 .. 3]».made;

		make ($condition ?? $true !! $false);
	}

	method symbol ($/)
	{
		# If this "symbol" is an expression evaluate it and return the result
		return make $<expression>.made if $<expression>;

		# Trim away whitespace
		my Str $value = ~$/.trim;

		# Return the symbol as a Num, if possible
		try return make $value.Num;

		# Return the defined value if it exists
		return make $!env.get-variable($value) if $!env.has-variable($value);

		# Leave it as-is, so the expression target can make use of it
		make $value;
	}
}

=begin pod

=NAME    Scheme::Parser::Actions
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 EXAMPLES

=head1 SEE ALSO

=end pod

# vim: ft=perl6 noet
