#! /usr/bin/env false

use v6.c;

grammar Scheme::Parser
{
	rule TOP { <expression> }
	rule expression { "(" <symbol>+ ")" }
	rule symbol { <-[ ( \s ) ]>+ | <expression> }
}

=begin pod

=NAME    Scheme::Parser
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.0.0

=head1 SYNOPSIS

=head1 DESCRIPTION

=head1 EXAMPLES

=head1 SEE ALSO

=end pod

# vim: ft=perl6 noet
